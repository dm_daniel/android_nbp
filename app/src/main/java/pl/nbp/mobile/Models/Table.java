package pl.nbp.mobile.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;
//klasa modelowa
public class Table {

//obsługujemy takie dane.
    @SerializedName("rates")
    private List<Rate> rates;

    //getter dla pobierania danych
    public List<Rate> getRates() {
        return rates;
    }
}
