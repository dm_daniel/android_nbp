package pl.nbp.mobile.Models;
//klasa modelowa( dla http://api.nbp.pl/
import com.google.gson.annotations.SerializedName;

public class Rate {

    //pola odpowiadające stronie api NBP. Musi odpowiadac strutkurze pliku JSON
    @SerializedName("currency")
    private String name;

    @SerializedName("code")
    private String code;
//mid-kurs średni
    @SerializedName("mid")
    private float mid;

    //gettery
    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public float getMid() {
        return mid;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", mid=" + mid +
                '}';
    }
}
