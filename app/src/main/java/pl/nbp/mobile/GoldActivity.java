package pl.nbp.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pl.nbp.mobile.Models.Gold;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoldActivity extends AppCompatActivity {

    //lista dni do wyboru
    List<Integer> days = Arrays.asList(10, 20, 30, 50, 90);

    //metoda do ustawienia wyglądu
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //podpinamy działalność dla golda
        setContentView(R.layout.activity_gold);
        setToolbar();

        //element na górze - lista rozwijalna
        setSpinner();
    }

    private void setSpinner() {
        Spinner spinner = findViewById(R.id.spinner);

        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,
                android.R.layout.simple_spinner_item,
                days);

        spinner.setAdapter(adapter);

        //Dodanie reakcji na wybór elementu ze spinnera
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //gdy wybrałem element, dostaje info który nr wiersza została wybrana ( ze spinnera ) -
                // odwołuje sie do Listy days z góry.
                fetchData(days.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    //FetchData - asynchronicznie pobiera dane
    private void fetchData(Integer numberOfElements) {
        Call<List<Gold>> call = NbpApi.getApi().getGold(numberOfElements);
        call.enqueue(getCallback());

    }

    //w zależności jakie dane przyjdą, wywoła się jedna z metod onResponse lub onFailure
    private Callback<List<Gold>> getCallback() {
        return new Callback<List<Gold>>() {
            @Override
            public void onResponse(Call<List<Gold>> call, Response<List<Gold>> response) {
                //jeśli dane przyszły
                if (response.body() != null && !response.body().isEmpty()) {
                    showData(response.body());
                }
            }

            //gdy sie nie uda
            @Override
            public void onFailure(Call<List<Gold>> call, Throwable t) {
                t.printStackTrace();
            }
        };
    }

    //metoda do wykresu
    private void showData(List<Gold> goldRates) {
        LineChart chart = findViewById(R.id.chart);
//czyścimy żeby się nie nakładały (nie potrzebne)
//        chart.clear();
//
        //lista entry com.github
        ArrayList<Entry> values = new ArrayList<>();

        //dostaje listę kursów i dodaję do listy entry. W pętli, która obraca się tyle razy ile jest rozmiaru tablicy
        for (int position = 0; position < goldRates.size(); position++) {
            Entry entry = new Entry(position, goldRates.get(position).getPrice());

            values.add(entry);
        }

        //łączenie zbioru w 1 linie
        LineDataSet set = new LineDataSet(values, "Kursy złota");
        //zaokrąglenie wykresu - w jaki sposób punkty mają być połączone
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        // set.setMode(LineDataSet.Mode.LINEAR);

        //ustawienie koloru linii i wypełnienia
        set.setColor(getResources().getColor(R.color.colorPrimaryDark));
        set.setFillColor(getResources().getColor(R.color.darkColor));

        //ustawienie wypełnienia pod linią na tak.
        set.setDrawFilled(true);
        //grubość lini łączącej punkty
        set.setLineWidth(6f);

//        zablokowanie kółek na wykresie
//        set.setDrawCircles(false);

        //tworzymy obiekt typu line data
        LineData data = new LineData(set);
        data.setValueTextSize(10f);

        chart.setData(data);
        //przerysowanie ponownie na bazie tego co ustawiłem
        chart.invalidate();
    }

    //działanie strzałki cofającej
    private void setToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("KURS GOLDA");

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }
    }

    //działanie strzałki cofającej
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
