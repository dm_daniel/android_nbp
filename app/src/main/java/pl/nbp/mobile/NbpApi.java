package pl.nbp.mobile;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import pl.nbp.mobile.Models.Gold;
import pl.nbp.mobile.Models.Table;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface NbpApi {
    //stały początek dla podstron api
    String BASE_URL = "http://api.nbp.pl/api/";

    @GET("exchangerates/tables/A")
    Call<List<Table>> getCurrencies();

    @GET("cenyzlota/last/{number}")
    Call<List<Gold>> getGold(@Path("number") int numberOfElements); //@Path - w ścieżce ma być numer dni

    //metoda zwracająca odpowiednio przetworzoną listę metod - zwraca obiekt
    static NbpApi getApi() {
        return NbpApi.getRetrofit().create(NbpApi.class);
    }

    //Metoda pracująca na obiektach retrofit
    static Retrofit getRetrofit() {

        //informacja w konsoli jakie dane wchodzą i wychodzą.Dodanie logu do pobierania danych
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);        //poziom informacji jaki chcemy otrzymywać w Logu
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();


        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //przetworzenie danych
                .build();

    }
}
