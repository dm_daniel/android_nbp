package pl.nbp.mobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutput;
import java.util.Currency;
import java.util.List;

import pl.nbp.mobile.Models.Rate;
import pl.nbp.mobile.Models.Table;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrencyActivity extends AppCompatActivity {
    private ProgressBar progressBar;

    //wywołuje sie w momencie startu aplikacji
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //podpięcie dizajnu
        setContentView(R.layout.activity_currency);

        //pobranie danych
        fetchData();
        //ustawienie toolbara (gornej belki aplikacji)
        setToolbar();
        //kręcące koło ładowania (trzeba zamknąć)
        progressBar = findViewById(R.id.progress_bar);
    }

    //działanie strzałki cofającej
    private void setToolbar() {
        if (getSupportActionBar() != null) {    //czy istnieje belka górna
            getSupportActionBar().setTitle("KURSY WALUT");  //ustawienie tytułu

            //dodanie pobsługi strzałki wstecz
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }
    }

    //działanie strzałki cofającej
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //obsługia kliknięcia strzałki w górnym menu
        if (item.getItemId() == android.R.id.home) {
            //zamknięcie obecnej aktywności
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    //metoda odwołuje się do interfejsu NbpApi
    private void fetchData() {
        Call<List<Table>> call = NbpApi.getApi().getCurrencies();
//wykonujemy akcje asynchronicznie w nowym wątku( wiec musze przekazac obiekt klasy Callback)
        call.enqueue(getCallback());
    }

    private Callback<List<Table>> getCallback() {

        //zwracam obiekt klasy anonimowej. Wymagane jest nadpisanie 2 metod
        return new Callback<List<Table>>() {
            @Override
            public void onResponse(Call<List<Table>> call, Response<List<Table>> response) {
                //usunięcie kółka -progress baru, jeśli dane przyszły
                progressBar.setVisibility(View.GONE);

                //sprawdzenie czy kod odpowiedzi jest poprawny ( w zakresie html )
                //tzn. w zakresie 200 - 299
                if (response.isSuccessful()
                        && response.body() != null
                        && !response.body().isEmpty()) {

                    //jeśli dane są w porządku wyświetlam je - przychodzi lista 1 elementowa ( wybieramy 1 listę z danymi np z zakresu 10 dni)
                    //przekazuje do metody showData zerową pozycję
                    showData(response.body().get(0));
                }
            }

            //gdy się coś nie uda:
            @Override
            public void onFailure(Call<List<Table>> call, Throwable t) {
                //usuniecie kółka
                progressBar.setVisibility(View.GONE);
                //wyświetlenie stack trace
                t.printStackTrace();
                String message = t.getMessage();

                //ustawiam text i zmieniam widoczność. Wypełnienie i wyświetlenie błędu
                TextView error = findViewById(R.id.error);
                error.setText(message);
                error.setVisibility(View.VISIBLE);
            }
        };
    }

    private void showData(Table table) {
        //łącze z widokiem
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        //tworzymy adapter i przekazujemy mu liste wierszy
        CurrencyAdapter adapter = new CurrencyAdapter(table.getRates());
        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //dodanie separatorów między liniami
        DividerItemDecoration divider =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);

        recyclerView.addItemDecoration(divider);

    }


}
