package pl.nbp.mobile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //nadpisana metoda do ładowania wyglądu
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Ustawienie widoku aplikacji z folderu layouts
        setContentView(R.layout.activity_main);

        setupUi();
    }

    //metoda do konfiguracji wyglądu
    private void setupUi() {
        //połączenie logiki z wyglądem aplikacji. Dostęp do 1 i 2 przycisku
        Button first = findViewById(R.id.first_button);
        Button second = findViewById(R.id.second_button);

        //inne opcje dla przycisków
      //  second.setVisibility(View.GONE);

        //nasluchiwacz naciśnięcia ( użycie klasy anonimowej, która implementuje metodę onClick ). Listener reagujący na krótkie kliknięcie
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast - wyświetlenie dymka z informacją
//                Toast.makeText(MainActivity.this,
//                        "Witamy w skarbcu",
//                        Toast.LENGTH_LONG)
//                        .show();

                //załadowanie działania
                Intent intent =
                        new Intent(MainActivity.this,
                                GoldActivity.class);

                //otwarcie nowej aktywności
                startActivity(intent);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //z jakiej do jakiej aktywności nastąpi otwarcie ( korzystamy z nowej klasy CurrencyActivity)
                Intent intent = new Intent(MainActivity.this,
                        CurrencyActivity.class);

                startActivity(intent);
            }
        });
    }
}
