package pl.nbp.mobile;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.nbp.mobile.Models.Rate;

//RecycleView - schody ruchome przy przewijaniu na telefonie
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.Row> {

    private List<Rate> rates;

     CurrencyAdapter(List<Rate> rates) {
        this.rates = rates;
    }

    //tworzenie danego wiersza. wywoła się tyle, razy ile mamy wierszy na ekranie
    @NonNull
    @Override
    public Row onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.currency_row, viewGroup, false);

        return new Row(view);
    }

    //do wypełniania treści, jak dany obiekt jest wyświetlany
    @Override
    public void onBindViewHolder(@NonNull Row row, int i) {
        //i = pozycja
        //z listy pobieram coś na konkretnej pozycji
        Rate rate = rates.get(i);
        //wypelnij wiersz tym konkretnym kursem (bind - złącz)
        row.bind(rate);
    }

    //ile elementów przygotować żeby zmieścić na ekranie
    @Override
    public int getItemCount() {
        return rates.size();
    }

    //klasa wewnętrzna do obsługi 1 wiersza
    class Row extends RecyclerView.ViewHolder {
        //pola, Elementy składowe wiersza
        private TextView name;
        private TextView description;
        private TextView mid;


        //konstruktor rodzica
        Row(@NonNull View itemView) {
            super(itemView);
//w konstruktorze znajduje elementy
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);
            mid = itemView.findViewById(R.id.mid);
        }

        //ustawiam wartości
        void bind(Rate rate) {
            name.setText(rate.getCode());
            description.setText(rate.getName());
            mid.setText(String.format("KURS: \n %f", rate.getMid()));
        }

    }

}
